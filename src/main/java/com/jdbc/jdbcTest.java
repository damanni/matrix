package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class jdbcTest {

	public static void main(String[] args) {
		
		String ojdbcUrl = "jdbc:oracle:thin:@localhost:1521:xe";
		String user = "matrix";
		String pass = "matrix";
		
		try {
			System.out.println("Connecting to database: " +ojdbcUrl);
			
			Connection myConn =
					DriverManager.getConnection(ojdbcUrl, user, pass);
			
			System.out.println("Connection Successful!!!");
		}
		catch(Exception exc) {
			exc.printStackTrace();
		}

	}

}
