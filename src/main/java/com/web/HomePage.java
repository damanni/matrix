package com.web;

import com.services.HomePageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class HomePage {
	
	@Autowired
	private HomePageService service;
	
	@RequestMapping(value = "/homepage")
	public String homepage() {
		return service.HomeMessage();
	}
}
