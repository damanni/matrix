-- Drop all sequences
DROP SEQUENCE seq_persons;
DROP SEQUENCE seq_users;
DROP SEQUENCE seq_addresses;
DROP SEQUENCE seq_address_types;
DROP SEQUENCE seq_phones;
DROP SEQUENCE seq_phone_types;
DROP SEQUENCE seq_states;

--Drop all tables
DROP TABLE persons_phone_numbers_xref;
DROP TABLE persons_addresses_xref;
DROP TABLE users;
DROP TABLE persons;
DROP TABLE phone_types;
DROP TABLE address_types;
DROP TABLE phone_numbers;
DROP TABLE addresses;
DROP TABLE states;

-- Create person table
CREATE TABLE persons (
    person_id INT PRIMARY KEY,
    first_name VARCHAR2(40) NOT NULL,
    last_name VARCHAR2(40) NOT NULL,
    time_created  TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
    time_updated TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
);

-- Create states table
CREATE TABLE states (
    state_id INT PRIMARY KEY,
    state_code CHAR(2) NOT NULL UNIQUE,
    state_name VARCHAR2(128) NOT NULL UNIQUE    
);

-- Create users table 
CREATE TABLE users (
    user_id INT PRIMARY KEY,
    user_name VARCHAR2(40) NOT NULL UNIQUE,
    password VARCHAR2(128) NOT NULL,
    -- 1 to 1 (user to person)
    person_id INT REFERENCES persons(person_id),
    time_created  TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
    time_updated TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
);

-- Address table
-- 1 to 1 address to type
CREATE TABLE addresses (
    address_id INT PRIMARY KEY,
    address_1 VARCHAR2(128) NOT NULL,
    address_2 VARCHAR2(128) NOT NULL,
    city VARCHAR2(60) NOT NULL,
    state_id INT NOT NULL REFERENCES states(state_id),
    zip INT NOT NULL,
    time_created  TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
    time_updated TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
);

-- Phone table (not sure if it should be this broken down)
CREATE TABLE phone_numbers (
    phone_id INT PRIMARY KEY,
    phone_area_code CHAR(3) NOT NULL,
    phone_prefix CHAR(3) NOT NULL,
    phone_line CHAR(4) NOT NULL,
    time_created  TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL,
    time_updated TIMESTAMP DEFAULT SYSTIMESTAMP NOT NULL
);

-- Address type table
CREATE TABLE address_types (
    address_type_id INT PRIMARY KEY,
    type VARCHAR2(24)
);

-- Phone type table
CREATE TABLE phone_types (
    phone_type_id INT PRIMARY KEY,
    type VARCHAR2(24)
);

-- Correct me on naming convention please
-- Cross Refernce Table (persons, addresses, address_types)
CREATE TABLE persons_addresses_xref (
    person_id NOT NULL REFERENCES persons(person_id),
    address_id NOT NULL REFERENCES addresses(address_id),
    address_type_id NOT NULL REFERENCES address_types(address_type_id),
    CONSTRAINT person_address_type_pk PRIMARY KEY (person_id, address_id, address_type_id)
);

-- Correct me on naming convention please
-- Cross Reference Table (persons, phone_numbers, phone_types
CREATE TABLE persons_phone_numbers_xref (
    person_id NOT NULL REFERENCES persons(person_id),
    phone_id NOT NULL REFERENCES phone_numbers(phone_id),
    phone_type_id NOT NULL REFERENCES phone_types(phone_type_id),
    CONSTRAINT person_phone_type_pk PRIMARY KEY (person_id, phone_id, phone_type_id)
);

-- Create Sequences with values 1 to 100 (100 unique for now)
CREATE SEQUENCE seq_users
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_persons
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_addresses
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_address_types
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_phones
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_phone_types
START WITH 1
INCREMENT BY 1
MAXVALUE 100
MINVALUE 1
CACHE 20
CYCLE
ORDER;

CREATE SEQUENCE seq_states
START WITH 1
INCREMENT BY 1
MAXVALUE 60
MINVALUE 1
CACHE 20
CYCLE
ORDER;


-- Insert into states table
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'AL', 'Alabama');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'AK', 'Alaska');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'AZ', 'Arizona');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'AR', 'Arkansas');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'CA', 'California');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'CO', 'Colorado');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'CT', 'Connecticut');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'DE', 'Delaware');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'DC', 'District of Columbia');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'FL', 'Florida');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'GA', 'Georgia');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'HI', 'Hawaii');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'ID', 'Idaho');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'IL', 'Illinois');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'IN', 'Indiana');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'IA', 'Iowa');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'KS', 'Kansas');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'KY', 'Kentucky');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'LA', 'Louisiana');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'ME', 'Maine');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MD', 'Maryland');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MA', 'Massachusetts');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MI', 'Michigan');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MN', 'Minnesota');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MS', 'Mississippi');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MO', 'Missouri');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'MT', 'Montana');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NE', 'Nebraska');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NV', 'Nevada');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NH', 'New Hampshire');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NJ', 'New Jersey');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NM', 'New Mexico');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NY', 'New York');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'NC', 'North Carolina');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'ND', 'North Dakota');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'OH', 'Ohio');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'OK', 'Oklahoma');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'OR', 'Oregon');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'PA', 'Pennsylvania');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'RI', 'Rhode Island');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'SC', 'South Carolina');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'SD', 'South Dakota');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'TN', 'Tennessee');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'TX', 'Texas');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'UT', 'Utah');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'VT', 'Vermont');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'VA', 'Virginia');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'WA', 'Washington');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'WV', 'West Virginia');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'WI', 'Wisconsin');
INSERT INTO states (state_id, state_code, state_name) VALUES (seq_states.NEXTVAL, 'WY', 'Wyoming');

INSERT INTO persons (person_id, first_name, last_name) VALUES (seq_persons.NEXTVAL, 'David', 'Manni');
INSERT INTO users (user_id, user_name, password, person_id) VALUES (seq_users.NEXTVAL, 'davidmanni', 
  'password', (select person_id from persons where last_name='Manni' and first_name='David'));
  
SELECT * FROM users;
SELECT * FROM persons;